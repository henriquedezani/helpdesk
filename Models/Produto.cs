namespace HelpDesk.Models
{
    public class Produto
    {
        public int IdProduto { get; set; }
        public string Nome { get; set; }
    }
}