using System.Data.SqlClient;

namespace HelpDesk.Repositories
{
    public abstract class RepositoryBase
    {
        protected SqlConnection connection;

        // abrir a conexão:
        public RepositoryBase()
        {
            string strConn = @"Data Source=localhost;
                                 Initial Catalog=BDHelpDesk;
                                 Integrated Security=true";

            connection = new SqlConnection(strConn);

            connection.Open();
        }

        // fechar a conexão:
        public void Dispose()
        {
            connection.Close();
        }
    }
}