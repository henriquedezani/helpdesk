using HelpDesk.Models;
using System.Data.SqlClient;

namespace HelpDesk.Repositories
{
    public class ProdutoRepository : RepositoryBase
    {
        public void Create(Produto e)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "insert into Produto values (@nome)";

            cmd.Parameters.AddWithValue("@nome", e.Nome);

            cmd.ExecuteNonQuery();
        }
    }
}